package thread;

public class MyThread3 extends Thread{
	public void run() {
		try {
			MyThread1().start();
			Thread.sleep(1000);
			MyThread2().start();
			Thread.sleep(1000);
			
		}catch(InterruptedException e) {}
	}
}

class MyThread1 extends Thread{
	public void run() {
		System.out.println("execution_thread_name");
	}
}

class MyThread2 extends Thread{
	public void run() {
		System.out.println("message_number");
	}
}



