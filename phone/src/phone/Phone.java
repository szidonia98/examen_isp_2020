package phone;

public class Phone {
	private String user;
	private boolean value;
	private String percentage;
	
	public Phone() {
		
	}
	public Phone(String user) {
		this.user=user;
	}
	public String getUser() {
		return user;
	}
	
	public static void main(String[] args) {
		Phone phone1=new Phone();
		
		
		
	}
}

class User { //dependency
	public void displayUser(Phone phone) {
		System.out.println(phone.getUser());
	}
}
class GPSModule{ //composition
	
}
class Button{ //composition
	
	public Button(){
		
	}
	public Button(boolean value) {
		if (value)
			System.out.println("the button is pressed");
		else
			System.out.println("the button is not pressed");
	}
	
	
}
class Battery{ //aggregation
	public Battery(String percentage) {
		System.out.println("Battery:"+percentage);
	}
}
class Display{ //aggregation
	
}
